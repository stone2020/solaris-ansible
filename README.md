# sample-data-repo

Ansible Role: os-check

Perform Health check on Linux system, generate  Health check report 

Req
This role is only performed on Linux systems.

Test environment
ansible 2.7.0 os Oracle 7 X64 python 2.7.5  Solaris 11.3 10

download filter_plugins  add follow to ansible.cfg  default model  to setting the filter_plugins :

filter_plugins     = /etc/ansible/filter_plugins

Role variables
check_day: "{{ '%Y-%m-%d' | strftime }}"
# Reportstore Dir
check_report_path: /tmp
check_report_file_suffix: "-{{ check_day }}"



Example Playbook
#!/bin/ansible-playbook
#--ask-become-pass 
---
- hosts: ora61
  gather_facts: true
  vars:
    check_report_path: /tmp
  roles:
    - os-check



**Implementation process**
Use the script files\check_sun.sh to execute remotely to obtain resource data and return it in a json structure.
Use the jinja2 template to render the acquired data into the template file templates\report-cssinline.html, and the generated file is stored in the specified directory.
report-cssinline.html is the html file where the css settings are stored inline, and report.html is the source template file. After modifying the source template file, use the Responsive Email CSS Inliner to convert to better compatible email display .
The get_check_data filter used in the template is to obtain the script execution results of each host from hostvars, analyze and sort them and pass them to the template, and use the passed data for rendering.
Obtain the content of the generated template file and send it to the recipient via smtp.
Statistical system resources
Hostname
Main IP
OS
CPU Used
CPU LoadAvg
Mem Used
Swap Used
Disk Size Used
Disk Inode Used
Tcp Connection Used
Timestamp
